variable "vpc_id" {}
variable "redis_url" {}
variable "mysql_host" {}
output "backend_url" {
  value = "https://backend"
}
