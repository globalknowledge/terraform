module "user" {
  source = "../modules/user"
  name                              = "waild.mansia.2"
  force_destroy                     = true
  pgp_key                           = "keybase:walidportago"
}
output "creds" {
  value = module.user.aws_iam_user-credentials
}